#include <iostream>
#include <conio.h>
#include <math.h>
using namespace std;
int main()
{
	
	int list;
	cout << "List Program:" << endl << "1. Menghitung Luas Segititga" << endl << "2. Program Aritmatika" << endl << "3. Menghitung Total Penjualan" << endl << endl;
	cout << "Ketik angka urutan program yang ingin dijalankan (1-3): ";
	cin >> list;
	
	if(list == 1)
	{
		cout << endl << "Menghitung Luas Segitiga" << endl << "-----------------------------" << endl;
		float a, t, luas;

		cout << "Masukkan panjang alas (cm) ";
		cin >> a;
		
		cout << "Masukkan panjang tingginya (cm) ";
		cin >> t;
	
		luas = a * t / 2;
		
		cout << "Luas Segitiganya adalah " << luas << " cm" << endl << "-----------------------------" << endl;
	} else if (list == 2){
		cout << endl << "Operator Aritmatika" << endl;
		int A, B, C, Y;
	
		cout << "Masukkan nilai A: ";
		cin >> A;
		
		cout << "Masukkan nilai B: ";
		cin >> B;
		
		cout << "Masukkan nilai C: ";
		cin >> C;
		
		Y = pow(A,2) + pow(B,2) + pow(C,3);
		
		cout << "Hasil Y = A^2 + B^2 + C^3 = " << Y << endl;
	} else if (list == 3){
		cout << endl << "Menghitung Total Penjualan" << endl << "-----------------------------" << endl;
		int harga, jumlah, beli, diskon, total;
	
		cout << "Masukkan Harga Barang: ";
		cin >> harga;
		
		cout << "Masukkan Jumlah Barang: ";
		cin >> jumlah;
		
		beli = harga * jumlah;
		
		if (jumlah > 100)
		{
			diskon = beli * 0.15;
			total = beli - diskon;
		} else
		{
			diskon = beli * 0.05;
			total = beli - diskon;
		}
		
		cout << "Total Pembelian: " << beli << endl;
		cout << "Diskon: " << diskon << endl;
		cout <<"Total Pembayaran: " << total << endl;
	} else {
		cout << "Harap masukkan angka urutan program yang ingin dijalankan (1-3): ";
		cin >> list;
	}
	
	getch();
	return 0;
}
